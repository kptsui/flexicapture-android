## FlexiCapture Android ##

This README document the steps are necessary to get the application up and running.

### Features ###
* Pre-processing of an image selected from Android mobile or taken by phone camera
* Cropping the processed image
* Upload the image to server for FlexiCapture to capture the text and get the result back from server

### Configuration
  * minSdkVersion 16 (Android 4.1)
  * targetSdkVersion 23 (Android 6.0)
  * compileSdkVersion 23


### How do I get set up? ###

```ssh
git clone https://kptsui@bitbucket.org/dev_wcl/flexicapture-android.git
```
* Or in Android Studio, select "Check out project from Version Control", enter https://kptsui@bitbucket.org/dev_wcl/flexicapture-android.git

* OpenCV is already installed in the project. :+1:
* Make sure NDK is installed in Android Studio.

### Used Web API ###
https://bitbucket.org/dev_wcl/flexicapture-asp.net-web-api

### HTTP Request Work Flow ###

Upload image to directory "\\HKITLAB\FCinput" of server.

Image upload request:

```json
{
	"success": false,
	"fileName": "",
	"image": "base64 image encode string"
}
```

Image upload successful response:

```json
{
	"success": true,
	"fileName": "JPEG_17_08_2016_1657127701_",
	"image": "\\HKITLAB\FCinput\JPEG_17_08_2016_1657127701_.jpg"
}
```

After uploaded, client will wait for couple seconds, then invoke method "requestResultHKID" in "HKIDUtil.java" to send request to server with JSON Object (Class HKID).

requestResultHKID:

```json
{
	"success": false,
	"processing": true,
	"fileName": "fileName return from Image Upload above",
	"HKID_DOB": "",
	"HKID_NameEng": "",
	"HKID_CardNo": "",
	"HKID_Sex": "",
	"HKID_Nationality": ""
}
```

If FlexiCapture processed the image successfully, it will output a CSV file storing its result in "\\HKITLAB\FCoutput",
otherwise the uploaded image will be created in "\\HKITLAB\FCunrecognized". 
So, the API is going check whether the folders contain a file with the name as "fileName" in JSON Object HKID.

On requestResultHKID responses, it will return the JSON Object HKID back.

If "processing" is true, it means both folders do not contain a file with the name as "fileName" in JSON Object HKID.
The client should wait 1 - 2 seconds and then make the request again to check the result.

If "processing" is false and "success" is true, it means a CSV file found in "\\HKITLAB\FCoutput" with the name as "fileName" in JSON Object HKID.
The API will set the fields of HKID in JSON Object according to CSV content and then return the JSON Object back to client.

Else, the client will end the process.

Note: The Mainland Travel Permit or other future process should be the same or very similiar. However, the FlexiCapture Template is not available yet..

### Image Processing Library Used:

 * [OpenCV](http://opencv.org/) [(How to install in Android)](https://www.youtube.com/watch?v=OTw_GIQNbD8)
 * [GPUImage for Android](https://github.com/CyberAgent/android-gpuimage)
 * [Android Image Cropper](https://github.com/ArthurHub/Android-Image-Cropper)