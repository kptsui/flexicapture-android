package org.bitbucket.kptsui.cap;

/**
 * Created by Simon_Tsui on 29/7/2016.
 */
public class HKID {
    public boolean success;
    public boolean processing;
    public String fileName;

    public String HKID_DOB;
    public String HKID_NameEng;
    public String HKID_CardNo;
    public String HKID_Sex;
    public String HKID_Nationality;
    
    public HKID(String fileName){
        success = false;
        processing = true;
        this.fileName = fileName;
        HKID_DOB = "";
        HKID_NameEng = "";
        HKID_CardNo = "";
        HKID_Sex = "";
        HKID_Nationality = "";
    }
}
