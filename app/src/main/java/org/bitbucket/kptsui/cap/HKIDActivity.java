package org.bitbucket.kptsui.cap;

import android.content.DialogInterface;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

public class HKIDActivity extends AppCompatActivity {

    public final static String BUNDLE_CLASS_TAG = "HKID";

    HKID HKIDCard;
    EditText birth;
    EditText name;
    EditText cardNo;
    EditText sex;
    EditText nation;

    // for resetAllFields
    String birthD, nameD, cardNoD, sexD, nationD;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_hkid);

        birth = (EditText) findViewById(R.id.HKID_DOB);
        name = (EditText) findViewById(R.id.HKID_NameEng);
        cardNo = (EditText) findViewById(R.id.HKID_CardNo);
        sex = (EditText) findViewById(R.id.HKID_Sex);
        nation = (EditText) findViewById(R.id.HKID_Nationality);

        Bundle bundle = getIntent().getExtras();
        HKIDCard = new Gson().fromJson(bundle.getString(BUNDLE_CLASS_TAG), HKID.class);
        birthD = HKIDCard.HKID_DOB;
        nameD = HKIDCard.HKID_NameEng;
        cardNoD = HKIDCard.HKID_CardNo;
        sexD = HKIDCard.HKID_Sex;
        nationD = HKIDCard.HKID_Nationality;
        resetAllFields(null);
    }

    @Override
    public void onBackPressed() {
        closeDialog(null);
    }

    public void resetAllFields(View v){
        birth.setText(birthD);
        name.setText(nameD);
        cardNo.setText(cardNoD);
        sex.setText(sexD);
        nation.setText(nationD);
    }

    public void closeDialog(View v){
        new AlertDialog.Builder(this)
                .setTitle("Cancel")
                .setMessage("Sure to close?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        HKIDActivity.this.finish();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }
}
