package org.bitbucket.kptsui.cap;

/**
 * Created by Simon_Tsui on 29/7/2016.
 */
public class Upload {
    public boolean success;
    public String image;
    public String fileName;

    public Upload(String base64){
        image = base64;
        success = false;
        fileName = "";
    }
}
