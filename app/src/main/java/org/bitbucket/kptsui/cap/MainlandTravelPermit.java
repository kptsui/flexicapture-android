package org.bitbucket.kptsui.cap;

/**
 * Created by Simon_Tsui on 4/8/2016.
 */
public class MainlandTravelPermit {
    public boolean success;
    public boolean processing;
    public String fileName;

    public String ChineseName;
    public String EnglishName;
    public String Surname;
    public String Given_Name;
    public String DOB;
    public String Gender;
    public String TravelNo;
}
