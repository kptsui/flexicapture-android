package org.bitbucket.kptsui.cap;

import android.content.DialogInterface;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

public class MainlandTravelActivity extends AppCompatActivity {

    public final static String BUNDLE_CLASS_TAG = "MainlandTravelPermit";

    MainlandTravelPermit mainlandTravelPermit;
    EditText birth;
    EditText nameChin;
    EditText nameEng;
    EditText cardNo;
    EditText sex;
    EditText surname;
    EditText givenName;

    // for resetAllFields
    String birthD, nameChinD, nameEngD, cardNoD, sexD, surnameD, givenNameD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_hrp);

        birth = (EditText) findViewById(R.id.DOB);
        nameChin = (EditText) findViewById(R.id.ChineseName);
        nameEng = (EditText) findViewById(R.id.EnglishName);
        cardNo = (EditText) findViewById(R.id.CardNo);
        sex = (EditText) findViewById(R.id.Gender);
        surname = (EditText) findViewById(R.id.Surname);
        givenName = (EditText) findViewById(R.id.Given_Name);

        Bundle bundle = getIntent().getExtras();
        mainlandTravelPermit = new Gson().fromJson(bundle.getString(BUNDLE_CLASS_TAG), MainlandTravelPermit.class);
        birthD = mainlandTravelPermit.DOB;
        nameChinD = mainlandTravelPermit.ChineseName;
        nameEngD = mainlandTravelPermit.EnglishName;
        cardNoD = mainlandTravelPermit.TravelNo;
        sexD = mainlandTravelPermit.Gender;
        surnameD = mainlandTravelPermit.Surname;
        givenNameD = mainlandTravelPermit.Given_Name;

        resetAllFields(null);
    }

    @Override
    public void onBackPressed() {
        closeDialog(null);
    }


    public void resetAllFields(View v){
        birth.setText(birthD);
        nameChin.setText(nameChinD);
        nameEng.setText(nameEngD);
        cardNo.setText(cardNoD);
        sex.setText(sexD);
        surname.setText(surnameD);
        givenName.setText(givenNameD);
    }

    public void closeDialog(View v){
        new AlertDialog.Builder(this)
                .setTitle("Cancel")
                .setMessage("Sure to close?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        MainlandTravelActivity.this.finish();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }
}
