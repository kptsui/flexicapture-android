package org.bitbucket.kptsui.cap;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ProcessTypeActivity extends AppCompatActivity {

    public enum ProcessType { HKID, MainlandTravelPermit};
    public static ProcessType type = ProcessType.HKID;

    ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_process_type);
        mListView = (ListView) findViewById(R.id.listView);

        mListView.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, ProcessType.values()));
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i){
                    case 0:
                        type = ProcessType.HKID;
                        break;
                    case 1:
                        type = ProcessType.MainlandTravelPermit;
                        break;
                    default:
                        break;
                }
                startActivity(new Intent(ProcessTypeActivity.this, MainActivity.class));
            }
        });
    }
}
