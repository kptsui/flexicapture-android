package org.bitbucket.kptsui.cap.Utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.bitbucket.kptsui.cap.Upload;
import org.bitbucket.kptsui.cap.HKID;
import org.bitbucket.kptsui.cap.MainActivity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

/**
 * Created by Simon_Tsui on 1/8/2016.
 */
public class HKIDUtil {

    public final static int CHECKING_INTERVAL_MS = 1000;
    public final static int TIME_OUT_SECOND = 20;
    public static long startRequestTime, startFcTime = 0;

    public static void uploadImage(final Context context, final RequestQueue requestQueue, final Bitmap bitmap) {
        final ProgressDialog loading = ProgressDialog.show(context, "Uploading...", "Please wait...", false, false);

        startRequestTime = System.currentTimeMillis();
        startFcTime = 0;
        Log.e("Calculate Time", "Start Upload");

        Log.i("ByteArrayOutputStream", "Start-----------------");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        final String base64 = Base64.encodeToString(imageBytes, Base64.NO_WRAP);
        Log.i("ByteArrayOutputStream", "End-----------------");
        Log.i("base64 string count", String.valueOf(base64.length()));

        JSONObject jsonBody  = new JSONObject();
        try {
            jsonBody.put("success", false);
            jsonBody.put("fileName", "");
            jsonBody.put("image", base64);

        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                WebAPI.UPLOAD_IMAGE,
                jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            final Upload upload = new Gson().fromJson(response.toString(), Upload.class);
                            Log.i("onResponse Upload", response.toString());
                            if(upload.success){
                                loading.setTitle("Processing...");
                                loading.setMessage("File checking...");
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            Log.e("Calculate Time", "End Upload: " + ((System.currentTimeMillis() - startRequestTime) / 1000) + " s");
                                            if(startFcTime == 0){
                                                startFcTime = System.currentTimeMillis();
                                                Log.e("Calculate Time", "Start FlexiCapture");
                                            }

                                            Log.i("Thread RequestFormData", "Thread.sleep(3000)");
                                            Thread.sleep(2500);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        requestResultHKID(context, requestQueue, loading, upload);
                                    }
                                }).start();
                            }
                            else{
                                loading.dismiss();
                                Toast.makeText(context, "Error: Server process failed", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        loading.dismiss();
                        Toast.makeText(context, "VolleyError", Toast.LENGTH_LONG).show();
                        Log.e("VolleyError", volleyError.toString());
                    }
                });

        request.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }

    public static void requestResultHKID(final Context context, final RequestQueue requestQueue, final ProgressDialog loading, final Upload upload) {
        Log.i("requestResultHKID", "Start request");

        JSONObject requestJson = null;
        try {
            if(upload == null){
                Toast.makeText(context, "Error: Upload data not exist", Toast.LENGTH_SHORT).show();
                return;
            }
            requestJson = new JSONObject(new Gson().toJson(new HKID(upload.fileName)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(requestJson == null) {
            loading.dismiss();
            return;
        }
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                WebAPI.GET_RESULT_HKID,
                requestJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            HKID HKIDCard = new Gson().fromJson(response.toString(), HKID.class);
                            Log.i("onResponse HKID", response.toString());

                            if((System.currentTimeMillis() - startFcTime) / 1000 > TIME_OUT_SECOND){
                                Toast.makeText(context, "Server no response, please try again later.", Toast.LENGTH_LONG).show();
                                Log.e("HKIDUtil", "Checking result time out");
                                loading.dismiss();
                                return;
                            }

                            if(HKIDCard.processing){
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            Log.e("Thread get Result HKID", "Thread.sleep(" + CHECKING_INTERVAL_MS + ")");
                                            Thread.sleep(CHECKING_INTERVAL_MS);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        requestResultHKID(context, requestQueue, loading, upload);
                                    }
                                }).start();
                            }
                            else if(HKIDCard.success){
                                Log.e("Calculate Time", "End FlexiCapture: " + ((System.currentTimeMillis() - startFcTime) / 1000) + " s");

                                loading.dismiss();
                                if(context instanceof MainActivity)
                                    ((MainActivity) context).toHKIDActivity(HKIDCard);
                            }
                            else{
                                Log.e("Calculate Time", "End FlexiCapture: " + ((System.currentTimeMillis() - startFcTime) / 1000) + " s");

                                loading.dismiss();
                                Toast.makeText(context, "Upload Failed: Invaild Image Source", Toast.LENGTH_LONG).show();
                                if(context instanceof MainActivity)
                                    ((MainActivity) context).toHKIDActivity(HKIDCard);
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        loading.dismiss();
                        if(volleyError instanceof TimeoutError){
                            new AlertDialog.Builder(context)
                                    .setTitle("Error")
                                    .setMessage("Connection time out, retry?")
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            loading.show();
                                            requestResultHKID(context, requestQueue, loading, upload);
                                        }
                                    })
                                    .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            loading.dismiss();
                                        }
                                    })
                                    .show();
                        }
                        else{
                            Toast.makeText(context, "VolleyError", Toast.LENGTH_LONG).show();
                        }
                        Log.e("VolleyError", volleyError.toString());
                    }
                });
        requestQueue.add(request);
    }
}
