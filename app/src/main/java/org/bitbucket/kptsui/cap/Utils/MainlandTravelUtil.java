package org.bitbucket.kptsui.cap.Utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.bitbucket.kptsui.cap.MainlandTravelPermit;
import org.bitbucket.kptsui.cap.Upload;
import org.bitbucket.kptsui.cap.HKID;
import org.bitbucket.kptsui.cap.MainActivity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

/**
 * Created by Simon_Tsui on 4/8/2016.
 */
public class MainlandTravelUtil {

    public static void uploadImage(final Context context, final RequestQueue requestQueue, final Bitmap bitmap) {
        final ProgressDialog loading = ProgressDialog.show(context, "Uploading...", "Please wait...", false, false);

        Log.i("ByteArrayOutputStream", "Start-----------------");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        final String base64 = Base64.encodeToString(imageBytes, Base64.NO_WRAP);
        Log.i("ByteArrayOutputStream", "End-----------------");
        Log.i("base64 string count", String.valueOf(base64.length()));

        JSONObject jsonBody  = new JSONObject();
        try {
            jsonBody.put("success", false);
            jsonBody.put("fileName", "");
            jsonBody.put("image", base64);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        //Log.i("jsonBody", jsonBody.toString());

        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                WebAPI.UPLOAD_IMAGE,
                jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            final Upload upload = new Gson().fromJson(response.toString(), Upload.class);
                            Log.i("onResponse Upload", response.toString());
                            if(upload.success){
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            Log.i("Thread get Result MTP", "Thread.sleep(3000)");
                                            Thread.sleep(3000);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        requestResultMTP(context, requestQueue, loading, upload);
                                    }
                                }).start();
                            }
                            else{
                                loading.dismiss();
                                Toast.makeText(context, "Error: Server process failed", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        loading.dismiss();
                        Toast.makeText(context, "VolleyError", Toast.LENGTH_LONG).show();
                        Log.e("VolleyError", volleyError.toString());
                    }
                });
        requestQueue.add(request);
    }

    public static void requestResultMTP(final Context context, final RequestQueue requestQueue, final ProgressDialog loading, final Upload upload) {
        Log.i("requestResultMTP", "Start request");
        JSONObject requestJson = null;
        try {
            if(upload == null){
                Toast.makeText(context, "Error: Upload data not exist", Toast.LENGTH_SHORT).show();
                return;
            }
            requestJson = new JSONObject(new Gson().toJson(new HKID(upload.fileName)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(requestJson == null) {
            loading.dismiss();
            return;
        }
        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.POST,
                WebAPI.GET_RESULT_MTP,
                requestJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            MainlandTravelPermit mtp = new Gson().fromJson(response.toString(), MainlandTravelPermit.class);
                            Log.i("onResponse HKID", response.toString());
                            if(mtp.processing){
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            Log.i("Thread get Result HKID", "Thread.sleep(2000)");
                                            Thread.sleep(2000);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        requestResultMTP(context, requestQueue, loading, upload);
                                    }
                                }).start();
                            }
                            else if(mtp.success){
                                loading.dismiss();
                                if(context instanceof MainActivity)
                                    ((MainActivity) context).toMainlandTravelActivity(mtp);
                            }
                            else{
                                loading.dismiss();
                                Toast.makeText(context, "Upload Failed: Invaild Image Source", Toast.LENGTH_LONG).show();
                                if(context instanceof MainActivity)
                                    ((MainActivity) context).toMainlandTravelActivity(mtp);
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        if(volleyError instanceof ServerError){
                            ErrorDialog(context, requestQueue, loading, upload, "Server Error, retry?");
                        }
                        else if(volleyError instanceof TimeoutError){
                            ErrorDialog(context, requestQueue, loading, upload, "Connection time out, retry?");
                        }
                        else{
                            loading.dismiss();
                            Toast.makeText(context, volleyError.toString(), Toast.LENGTH_LONG).show();
                        }
                        Log.e("VolleyError", volleyError.toString());
                    }
                });

        requestQueue.add(request);
    }

    public static void ErrorDialog(final Context context, final RequestQueue requestQueue, final ProgressDialog loading, final Upload upload, String msg){
        loading.dismiss();
        new AlertDialog.Builder(context)
                .setTitle("Error")
                .setMessage(msg)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        loading.show();
                        requestResultMTP(context, requestQueue, loading, upload);
                    }
                })
                .setNegativeButton(android.R.string.cancel, null)
                .show();
    }
}
