package org.bitbucket.kptsui.cap;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.bitbucket.kptsui.cap.Utils.HKIDUtil;
import org.bitbucket.kptsui.cap.Utils.MainlandTravelUtil;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import uk.co.senab.photoview.PhotoViewAttacher;
import org.bitbucket.kptsui.cap.ProcessTypeActivity.ProcessType;

public class MainActivity extends AppCompatActivity {

    static {
        if (!OpenCVLoader.initDebug()) {
            Log.d("OpenCV", "OpenCV is not loaded");
        } else {
            Log.d("OpenCV", "OpenCV Loaded");
        }
    }

    public final static int SELECT_FILE = 0;
    public final static int REQUEST_TAKE_PHOTO = 2;

    private String mCurrentPhotoPath;

    private PhotoViewAttacher mAttacher;
    private ImageView mImageView;
    private Uri imageUri;
    public Bitmap bitmapCache;
    public Bitmap mBitmap;
    private RequestQueue requestQueue;
    private android.os.Handler handler;

    public final static String TAG = "MainActivity";
    public Mat mat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Dexter.initialize(this);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setSubtitle(ProcessTypeActivity.type.toString());

        requestQueue = Volley.newRequestQueue(this);
        handler = new android.os.Handler();

        mImageView = (ImageView) findViewById(R.id.imageView);
        mAttacher = new PhotoViewAttacher(mImageView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.crop:
                toCropActivity(imageUri);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void btnImageClicked(View v){
        /*Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);*/

        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(intent, SELECT_FILE);
    }

    public void btnUploadClicked(View v){
        if(mBitmap != null){
            if(ProcessTypeActivity.type == ProcessType.HKID){
                HKIDUtil.uploadImage(MainActivity.this, requestQueue, mBitmap);
            }
            else if(ProcessTypeActivity.type == ProcessType.MainlandTravelPermit){
                MainlandTravelUtil.uploadImage(MainActivity.this, requestQueue, mBitmap);
            }
        }
        else
            Log.i("btn Upload Clicked", "Upload Clicked, is mBitmap null: " + (mBitmap == null));
    }
    public void btnCameraClicked(View v){
        Dexter.checkPermission(new PermissionListener() {
            @Override public void onPermissionGranted(PermissionGrantedResponse response) {
                dispatchTakePictureIntent();
            }
            @Override public void onPermissionDenied(PermissionDeniedResponse response) {/* ... */}
            @Override public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {/* ... */}
        }, Manifest.permission.CAMERA);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if( resultCode != RESULT_OK)
            return;

        Dexter.checkPermission(new PermissionListener() {
            @Override public void onPermissionGranted(PermissionGrantedResponse response) {
                Log.i(TAG, "onPermissionGranted, proceed onActivityResult");

                if (requestCode == REQUEST_TAKE_PHOTO) {
                    if (mCurrentPhotoPath != null) {
                        photoTakenResult();
                        mCurrentPhotoPath = null;
                    }
                }
                else if (requestCode == SELECT_FILE){
                    onSelectFromGalleryResult(data);
                }

                else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                    final ProgressDialog progressDialog = ProgressDialog.show(MainActivity.this, "Processing...", "Please wait...", false, false);

                    try {
                        CropImage.ActivityResult result = CropImage.getActivityResult(data);
                        Uri uri = result.getUri();
                        bitmapCache = MediaStore.Images.Media.getBitmap(MainActivity.this.getContentResolver(), uri);
                        bitmapCache = resizeBitmap(bitmapCache);
                        imageUri = Uri.fromFile(saveImage(bitmapCache));
                        updateImageView(bitmapCache);
                    } catch (IOException e) {
                        e.printStackTrace();
                        Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    } finally {
                        progressDialog.dismiss();
                    }
                }
            }
            @Override public void onPermissionDenied(PermissionDeniedResponse response) {/* ... */}
            @Override public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {/* ... */}
        }, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    // Use only this method to update ImageView
    // Do not use other method to update the ImageView
    private void updateImageView(Bitmap bitmap){
        if(bitmap == null) return;
        mBitmap = bitmap;
        mImageView.setImageBitmap(mBitmap);
        mAttacher.update();
    }

    public Bitmap resizeBitmap(Bitmap bitmap){
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if(width > 2000 && height > 1100){
            width = width / 2;
            height = height / 2;
        }
        else if(width > 1800 && height > 900){
            width = width * 2/3;
            height = height * 2/3;
        }
        else if(width > 1600 && height > 700){
            width = width * 4/5;
            height = height * 4/5;
        }
        else if(width > 1300 && height > 700){
            width = width * 9/10;
            height = height * 9/10;
        }

        if(bitmap.getWidth() != width){
            bitmap = Bitmap.createScaledBitmap(bitmap, width, height, false);
            Log.i(TAG, "Resize Bitmap - width: " + width + ", height: " + height);
        }
        return bitmap;
    }

    private File createImageFile() throws IOException {
        String imageFileName = "JPEG_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + "_";

        File dir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (!dir.exists())
        {
            Log.e("!dir.exists", "" + !dir.exists());
            if (!dir.mkdirs())
            {
                Log.e("!dir.mkdirs", "" + !dir.mkdirs());
            }
        }
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                dir
        );

        mCurrentPhotoPath = image.getAbsolutePath();

        Log.i("image", image.getAbsolutePath());
        Log.i("mCurrentPhotoPath", mCurrentPhotoPath);
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                showErrorDialog(ex);
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Log.i("photoFile != null", "" + (photoFile != null));

                Uri photoURI = FileProvider.getUriForFile(this,
                        "org.bitbucket.kptsui.cap",
                        photoFile);

                Log.i("photoURI", photoURI.toString());

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    private void photoTakenResult(){
        final ProgressDialog progressDialog = ProgressDialog.show(this, "Processing...", "Please wait...", false, false);
        setPic();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    PreProcess();
                    final File savedImage = saveImage(bitmapCache);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            if(savedImage != null){
                                toCropActivity(Uri.fromFile(savedImage));
                            }
                        }
                    });
                } catch (final Exception e) {
                    e.printStackTrace();
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        }).start();
    }

    private void onSelectFromGalleryResult(final Intent data) {
        final ProgressDialog progressDialog = ProgressDialog.show(this, "Processing...", "Please wait...", false, false);
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (data != null) {
                    try {
                        bitmapCache = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                        PreProcess();
                        final File savedImage = saveImage(bitmapCache);
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                if(savedImage != null){
                                    toCropActivity(Uri.fromFile(savedImage));
                                }
                            }
                        });
                    } catch (final Exception e) {
                        e.printStackTrace();
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }
            }
        }).start();
    }

    private File saveImage(final Bitmap bitmap){
        String dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString() + "/Preprocess";
        File myDir = new File(dir);
        myDir.mkdirs();

        String fileName = "FC_" + System.currentTimeMillis() + ".jpg";
        File file = new File(myDir, fileName);

        if (file.exists()) file.delete();
        Log.i(TAG, file.toString());

        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);

            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri contentUri = Uri.fromFile(file);
            mediaScanIntent.setData(contentUri);
            MainActivity.this.sendBroadcast(mediaScanIntent);

            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            file = null;
            Log.e(TAG, "save image failed, file return null");
        }
        return file;
    }

    private void setPic() {
        // Get the dimensions of the View
        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        //BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        bitmapCache = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
    }

    public void showErrorDialog(Exception e){
        new AlertDialog.Builder(this)
                .setTitle("Error")
                .setMessage(e.getMessage() + "\n" + e.toString())
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void toHKIDActivity(HKID hkid){
        Bundle bundle = new Bundle();
        bundle.putString(HKIDActivity.BUNDLE_CLASS_TAG, new Gson().toJson(hkid));
        Intent intent = new Intent(MainActivity.this, HKIDActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void toMainlandTravelActivity(MainlandTravelPermit mainlandTravelPermit){
        Bundle bundle = new Bundle();
        bundle.putString(MainlandTravelActivity.BUNDLE_CLASS_TAG, new Gson().toJson(mainlandTravelPermit));
        Intent intent = new Intent(MainActivity.this, MainlandTravelActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void toCropActivity(Uri uri) {
        if(!isExternalStorageWritable()){
            return;
        }

        if(uri != null && new File(uri.getPath()).exists()){
            Log.i(TAG, "File exists, go to CropActivity");
            CropImage.activity(uri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setBorderLineColor(Color.LTGRAY)
                    .setGuidelinesColor(Color.LTGRAY)
                    .setBorderCornerColor(Color.LTGRAY)
                    .start(MainActivity.this);
        }
        else{
            Toast.makeText(this, "Image file not found", Toast.LENGTH_LONG).show();
        }
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if(!Environment.MEDIA_MOUNTED.equals(state)){
            Toast.makeText(MainActivity.this, "The storage is not writable", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    public void PreProcess(){
        Log.i(TAG, "onPermissionGranted, PreProcess()");

        if(ProcessTypeActivity.type == ProcessType.HKID){
            PreprocessHKID();
        }
        else if(ProcessTypeActivity.type == ProcessType.MainlandTravelPermit){
            PreprocessMainlandTravelPermit();
        }
    }

    public void PreprocessHKID(){
        mat = new Mat(new Size(bitmapCache.getWidth(), bitmapCache.getHeight()), CvType.CV_8U);
        Utils.bitmapToMat(bitmapCache, mat);
        mat.convertTo(mat, -1, 1.9, 70); // 80

        Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2GRAY);
        Imgproc.GaussianBlur(mat, mat, new Size(3, 3), 0);
        Imgproc.adaptiveThreshold(mat, mat, 255, Imgproc.ADAPTIVE_THRESH_MEAN_C ,Imgproc.THRESH_BINARY, 99, 4);

        Utils.matToBitmap(mat, bitmapCache);
    }

    public void PreprocessMainlandTravelPermit(){
        // GPUImage Filter
        /*List<GPUImageFilter> filters = new LinkedList<>();
        filters.add(new GPUImageExposureFilter(exposure));
        filters.add(new GPUImageSaturationFilter(saturation));
        filters.add(new GPUImageContrastFilter(contrast));

        GPUImage gpuImage = new GPUImage(MainActivity.this);
        gpuImage.setImage(bitmapCache);
        gpuImage.setFilter(new GPUImageFilterGroup(filters));
        bitmapCache = gpuImage.getBitmapWithFilterApplied();*/

        mat = new Mat(new Size(bitmapCache.getWidth(), bitmapCache.getHeight()), CvType.CV_8U);
        Utils.bitmapToMat(bitmapCache, mat);
        mat.convertTo(mat, -1, 1.9, 70); // 80

        Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2GRAY);
        Imgproc.GaussianBlur(mat, mat, new Size(3, 3), 0);
        Imgproc.adaptiveThreshold(mat, mat, 255, Imgproc.ADAPTIVE_THRESH_MEAN_C ,Imgproc.THRESH_BINARY, 99, 4);

        Utils.matToBitmap(mat, bitmapCache);

        Dilate();
        Erode();
        //Erode();

        //Imgproc.blur(mat, mat, new Size(3, 3));
        //Core.normalize(mat, mat, 0, 255, Core.NORM_MINMAX);
    }

    public void Erode(){
        //int size = Imgproc.MORPH_ERODE;
        Imgproc.erode(mat, mat, new Mat());
    }

    public void Dilate(){
        int size = Imgproc.MORPH_DILATE;
        Imgproc.dilate(mat, mat, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size( 2*size + 1, 2*size+1 ), new Point( size, size ) ));
    }
}
